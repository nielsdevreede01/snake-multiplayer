package Main;

import Models.*;
import WebSocketModels.WebSocketAction;
import WebSocketModels.WebSocketMessage;
import WebSocketModels.WebSocketMessageFactory;
import WebSocketModels.WebSocketStatus;
import com.google.gson.Gson;

import javax.websocket.Session;
import java.io.IOException;
import java.util.List;

public class GameLogic {
    private IGame game;
    private GameData lastGameUpdate;
    private ServerEndpoint communicator;
    private boolean hasAPlayerDied = false;

    public GameLogic(boolean mode) {
        if (mode) {
            this.game = new Game();
        }
        this.communicator = ServerEndpoint.getInstance();
    }

    public void sendGameUpdate(List<Player> players, Tile levelUp, Lobby lobby) throws IOException {
        GameData data = new GameData(players, levelUp);
        WebSocketMessage msg = WebSocketMessageFactory.createWebSocketMessage(WebSocketAction.GAME_UPDATE, WebSocketStatus.SUCCESS, null, data);
        this.communicator.sendGameUpdate(msg, lobby);
    }

    public void setLastGameUpdate(List<Player> players, Tile levelUp){
        this.lastGameUpdate = new GameData(players, levelUp);
    }
    public GameData getLastGameUpdate() {
        return this.lastGameUpdate;
    }

    public IGame getGame(){
        return this.game;
    }

    public Lobby joinLobby(Lobby lobby, Session session) throws IOException {
        lobby = game.joinLobby(lobby);
        WebSocketMessage returnJoinLobby = WebSocketMessageFactory.createWebSocketMessage(WebSocketAction.PLAYER_JOINED, WebSocketStatus.SUCCESS, null, lobby);
        communicator.sendPlayerJoined(returnJoinLobby, session);
        if (lobby.getPlayers().size() >= 2) {
            WebSocketMessage returnJoinLobbyOther = WebSocketMessageFactory.createWebSocketMessage(WebSocketAction.OTHER_PLAYER_JOINED_LOBBY, WebSocketStatus.SUCCESS, null, lobby);
            communicator.sendOtherPlayerJoined(lobby, returnJoinLobbyOther);
        }
        return lobby;
    }

    public void checkIfGameCanStart(Lobby lobby) throws IOException {
        if (lobby.getPlayers().size() >= 2) {
            game.startGame(lobby, 0, 0);
            communicator.startGame(lobby, WebSocketMessageFactory.createWebSocketMessage(WebSocketAction.START_GAME, WebSocketStatus.SUCCESS, null, lobby));
        }
    }

    public void startGame(Lobby lobby, int x, int y) {
        game.startGame(lobby, x, y);
    }

    public void setPlayerDied(Player player, Lobby lobby) throws IOException {
        this.hasAPlayerDied = true;
        communicator.playerDied(player, lobby);
    }

    public boolean getHasAPlayerDied(){
        return this.hasAPlayerDied;
    }
}
