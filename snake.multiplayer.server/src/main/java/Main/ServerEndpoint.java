package Main;

import Models.*;
import WebSocketModels.WebSocketAction;
import WebSocketModels.WebSocketMessage;
import WebSocketModels.WebSocketStatus;
import com.google.gson.Gson;

import javax.websocket.*;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.*;

@javax.websocket.server.ServerEndpoint(value = "/game/")
public class ServerEndpoint {

    private static ServerEndpoint instance;

    public static ServerEndpoint getInstance() {
        if (instance != null) {
            return instance;
        }
        return new ServerEndpoint();
    }


    IGame game; // = new Game();
    private Gson gson;
    private static final Map<Session, Player> sessions = new HashMap<>();
    // Map of each list of sessions that are subscribed to that channel
    private static final Map<Integer, List<Session>> channels = new HashMap<>();

    public ServerEndpoint() {
        gson = new Gson();
        instance = this;
        game = new Game();
    }

    @OnOpen
    public void onConnect(Session session) {

        sessions.put(session, new Player(null));
        System.out.println("new connection: sessionID: " + session.getId());
        System.out.println("Total number of sessions: " + sessions.size());
    }

    @OnMessage
    public void onText(String message, Session session) throws IOException {
        System.out.println("[WebSocket Session ID] : " + session.getId() + " [Received] : " + message);
        handleMessageFromClient(message, session);
    }

    @OnClose
    public void onClose(CloseReason reason, Session session) {
        System.out.println("[WebSocket Session ID] : " + session.getId() + " [Socket Closed]: " + reason);
        sessions.remove(session);
    }

    @OnError
    public void onError(Throwable cause, Session session) {
        System.out.println("[WebSocket Session ID] : " + session.getId() + "[ERROR]: ");
        cause.printStackTrace(System.err);
    }


    private void handleMessageFromClient(String message, Session session) throws IOException {
        WebSocketMessage msg = null;
        try {
            msg = gson.fromJson(message, WebSocketMessage.class);
        } catch (Exception e) {
            WebSocketMessage returnMsg = new WebSocketMessage(null, WebSocketStatus.ERROR, "Could not parse message", null);
            session.getBasicRemote().sendText(gson.toJson(returnMsg));

        }
        assert msg != null;
        switch (msg.getAction()) {
            case CREATE_LOBBY:
                String name = msg.getData();

                List<Lobby> allLobbies = game.createLobby(name);
                Lobby createdLobby = allLobbies.get(allLobbies.size() - 1);

                channels.put(createdLobby.getId(), new ArrayList<>());
                channels.get(createdLobby.getId()).add(session);

                WebSocketMessage returnMessage = new WebSocketMessage(WebSocketAction.CREATE_LOBBY, WebSocketStatus.SUCCESS, null, gson.toJson(allLobbies));
                for (Session sess : sessions.keySet()) {
                    sess.getBasicRemote().sendText(gson.toJson(returnMessage));
                }
                break;
            case JOIN_LOBBY:
                String data = msg.getData();
                Lobby joinLobby = gson.fromJson(data, Lobby.class);
                GameLogic gameLogic = new GameLogic(true);
                joinLobby = gameLogic.joinLobby(joinLobby, session);
                channels.get(joinLobby.getId()).add(session);
                gameLogic.checkIfGameCanStart(joinLobby);
                System.out.println("LOBBY SIZE = " + joinLobby.getPlayers().size());
                break;
            case GET_LOBBIES:
                WebSocketMessage getAllLobbies = new WebSocketMessage(WebSocketAction.GET_LOBBIES, WebSocketStatus.SUCCESS, null, gson.toJson(Lobby.allLobbies));
                session.getBasicRemote().sendText(gson.toJson(getAllLobbies));
                break;
            case CHANGE_DIRECTION:
                data = msg.getData();
                DirectionData directionData = gson.fromJson(data, DirectionData.class);
                game.changeDirection(directionData.getDir(), directionData.getLobby(), directionData.getCurrentPlayer());
                break;
        }
    }

    public void sendGameUpdate(WebSocketMessage msg, Lobby lobby) throws IOException {
        if (msg != null && lobby != null && channels.get(lobby.getId()) != null) {
            for (Session sess : channels.get(lobby.getId())) {
                sess.getBasicRemote().sendText(gson.toJson(msg));
            }
        }

    }


    public void playerDied(Player player, Lobby lobby) throws IOException {
        WebSocketMessage msg = new WebSocketMessage(WebSocketAction.PLAYER_DIED, WebSocketStatus.SUCCESS, "Player died", gson.toJson(player));
        if (channels.get(lobby.getId()) != null)
            for (Session sess : channels.get(lobby.getId())) {
                sess.getBasicRemote().sendText(gson.toJson(msg));
            }
    }

    public void sendPlayerJoined(WebSocketMessage msg, Session session) throws IOException {
        if (session != null) {
            session.getBasicRemote().sendText(gson.toJson(msg));
        }
    }

    public void sendOtherPlayerJoined(Lobby lobby, WebSocketMessage msg) throws IOException {
        try {
            for (Session sess : channels.get(lobby.getId())) {
                sess.getBasicRemote().sendText(gson.toJson(msg));
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

    public void startGame(Lobby lobby, WebSocketMessage msg) throws IOException {
        try {
            for (Session sess : channels.get(lobby.getId())) {
                sess.getBasicRemote().sendText(gson.toJson(msg));
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }
}
