package WebSocketModels;

import com.google.gson.Gson;

public class WebSocketMessageFactory {
    private static Gson gson = new Gson();

    public static WebSocketMessage createWebSocketMessage(WebSocketAction action, WebSocketStatus status, String message, Object data){
        try{
            return new WebSocketMessage(action, status, message, gson.toJson(data));
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
        return null;
    }
}
