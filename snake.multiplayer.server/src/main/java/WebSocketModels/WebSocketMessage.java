package WebSocketModels;

public class WebSocketMessage {
    private String data;
    private WebSocketAction action;
    private WebSocketStatus status;
    private String message;

    public WebSocketMessage(WebSocketAction action, WebSocketStatus status, String message, String data) {
        this.action = action;
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public WebSocketAction getAction() {
        return action;
    }

    public WebSocketStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

}
