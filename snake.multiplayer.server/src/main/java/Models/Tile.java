package Models;

public class Tile {
    private int xPos;
    private int yPos;

    public Tile(int xPos, int yPos){
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public int getYPos(){
        return this.yPos;
    }
    public int getXPos(){
        return this.xPos;
    }
}
