package Models;


import Main.GameLogic;
import Main.ServerEndpoint;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Game implements IGame{
    private Random rnd = new Random();
    private int GRID_SIZE = 55;
    private Tile levelUp;
    private Timer timer;
    private int Speed = 75;
    private static Lobby lobby;
    private GameLogic gameLogic;

    @Override
    public Lobby joinLobby(Lobby lobby) {
        Lobby joinLobby = Lobby.allLobbies.get(Lobby.getLobbyById(lobby.getId()));
        lobby.getPlayers().get(0).setAnId();
        joinLobby.addPlayer(lobby.getPlayers().get(0));
        return joinLobby;
    }

    public void setLevelUp(Tile tile){
        this.levelUp = tile;
    }

    public Game(){
        this.gameLogic = new GameLogic(false);
    }
    public Game(boolean testing){
        this.gameLogic = new GameLogic(false);
        this.testing = testing;
    }

    @Override
    public List<Lobby> createLobby(String name) {
        if (name != null){
            Lobby lobby = new Lobby(name);
            Lobby.allLobbies.add(lobby);
        }
        return Lobby.allLobbies;
    }

    @Override
    public GameLogic getGameLogic(){
        return this.gameLogic;
    }
    public Game getGame(){
        return this;
    }

    @Override
    public Lobby startGame(Lobby lobby, int x, int y) {
        this.lobby = lobby;
        for(Player player: lobby.getPlayers()){
            if (x != 0 && y != 0) {
                player.initializeSnake(GRID_SIZE - 20, x, y);
            }else{
                player.initializeSnake(GRID_SIZE, rnd.nextInt(GRID_SIZE), rnd.nextInt(GRID_SIZE));
            }
        }
        createLevelUp();
        TimerTask task = new TimerTask() {
            public void run() {
                try {
                    updateTimer();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        timer = new Timer();
        timer.scheduleAtFixedRate(task, Speed, Speed);
        return lobby;
    }

    @Override
    public void changeDirection(Direction dir, Lobby lobby, Player player) {
        for (Player plyr : this.lobby.getPlayers()){
            if (plyr.getId() == player.getId()){
            plyr.changeDirection(dir);
        }
    }
    }


    private boolean debugging = true;
    private boolean testing;

    private void updateTimer() throws IOException {
        boolean didAPlayerDie = false;
        int counter = 0;
        for (Player player: lobby.getPlayers()) {
            if (counter != 1 && debugging){
                Tile firstTile = player.getFirstTile();
                if (firstTile.getXPos() == levelUp.getXPos() && firstTile.getYPos() == levelUp.getYPos()) {
                    player.addTileToSnake();
                    createLevelUp();
                }
                player.updateSnake();
                if (!checkIfPlayerIsAlive(player) || checkIfPlayersCollide(player)) {
                    didAPlayerDie = true;
                    timer.cancel();
                    gameLogic.setPlayerDied(player, lobby);
                    restartGame();
                    System.out.println("PLAYER IS DEAD");
                }
            }
            counter++;
        }
        if (!didAPlayerDie){
            gameLogic.setLastGameUpdate(lobby.getPlayers(), levelUp);
            if (!testing){
                gameLogic.sendGameUpdate(lobby.getPlayers(), levelUp, lobby);
            }
        }
    }

    private boolean checkIfPlayersCollide(Player player) {
        for (Player players : lobby.getPlayers()){
            if (players.getId() != player.getId()){
                for(Tile player2Tile : players.getSnakeTiles()){
                    if (player2Tile.getXPos() == player.getFirstTile().getXPos() && player2Tile.getYPos() == player.getFirstTile().getYPos()){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void restartGame() {
        startGame(this.lobby, 0 ,0);
    }

    private boolean checkIfPlayerIsAlive(Player player) {
        List<Tile> allSnakeTiles = player.getSnakeTiles();
        Tile firstTile = player.getFirstTile();
        int index = 0;
        for (Tile bodyTile: allSnakeTiles) {
            if (bodyTile.getYPos() == firstTile.getYPos() &&
                    bodyTile.getXPos() == firstTile.getXPos() &&
                    index != 0){
                return false;
            }
            index++;
        }

        return !checkIfPlayerIsOutSideBounds(player);
    }

    private boolean checkIfPlayerIsOutSideBounds(Player player) {
        Tile firstTile = player.getFirstTile();
        return firstTile.getXPos() > GRID_SIZE ||
                firstTile.getXPos() < 0 ||
                firstTile.getYPos() > GRID_SIZE ||
                firstTile.getYPos() < 0;
    }

    private void createLevelUp() {
        int xPos = rnd.nextInt(GRID_SIZE - 1);
        int yPos = rnd.nextInt(GRID_SIZE - 1);
        this.levelUp = new Tile(xPos, yPos);
    }
}
