package Models;

import java.util.List;
import java.util.Random;

public class Player {

    public static int idCounter = 0;
    Random rnd;
    private int id;
    private Snake snake;
    private String name;
    private int score;

    public Player(String name){
        this.id = idCounter++;
        this.name = name;
    }

    public void initializeSnake(int size,int x ,int y){
        this.rnd = new Random();
        this.snake = new Snake();
        snake.addTileToSnake(x,y);
    }

    public void updateSnake(){
        snake.moveForward();
    }

    public Tile getFirstTile(){
        return snake.getFirstTile();
    }

    public void addTileToSnake() {
        snake.addTileToSnake();
    }

    public List<Tile> getSnakeTiles() {
        return snake.getSnakeTiles();
    }

    public void changeDirection(Direction dir) {
        snake.changeDirection(dir);
    }

    public int getId(){
        return this.id;
    }

    public void setAnId() {
        this.id = idCounter++;
    }

    public Direction getDirection(){ return this.snake.getDirection(); }
}
