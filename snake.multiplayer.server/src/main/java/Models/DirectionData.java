package Models;

public class DirectionData {
    private Player currentPlayer;
    private Direction dir;
    private Lobby lobby;

    public Direction getDir() {
        return dir;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Lobby getLobby() {
        return lobby;
    }
}
