package Models;

public enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN
}
