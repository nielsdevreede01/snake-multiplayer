package Models;

import java.util.ArrayList;
import java.util.List;

public class Lobby {
    public static List<Lobby> allLobbies = new ArrayList<>();
    private static int idCounter = 0;

    public static int getLobbyById(int id){
        int counter = 0;
        for (Lobby lobby : allLobbies ) {
            if (lobby.getId() == id){
                return counter;
            }
            counter++;
        }
        return 0;
    }

    private String name;
    private int id;
    List<Player> players = new ArrayList<>();

    public Lobby(String name){
        this.name = name;
        this.id = idCounter++;
    }

    public String getName() {
        return this.name;
    }
    public int getId(){
        return this.id;
    }

    public void addPlayer(Player player){
        this.players.add(player);
    }

    public List<Player> getPlayers() {
        return players;
    }
}
