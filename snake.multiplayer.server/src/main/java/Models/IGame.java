package Models;

import Main.GameLogic;

import java.util.List;

public interface IGame {
    Lobby joinLobby(Lobby lobby);
    List<Lobby> createLobby(String name);
    Lobby startGame(Lobby lobby, int x , int y);
    void changeDirection(Direction dir, Lobby lobby, Player player);
    GameLogic getGameLogic();
    Game getGame();
    void setLevelUp(Tile tile);


}
