package Models;

import java.util.List;

public class GameData {
    private List<Player> players;
    private Tile levelUp;

    public GameData(List<Player> players, Tile levelUp){
        this.levelUp = levelUp;
        this.players = players;
    }

    public List<Player> getPlayers() {
        return players;
    }
}
