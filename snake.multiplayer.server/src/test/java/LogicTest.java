import Main.GameLogic;
import Models.*;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import javax.websocket.Session;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class LogicTest {
    private IGame game;
    private GameLogic logic;


    //WEKRT OP EEN OF ANDERE MANIER NIET!
    @BeforeEach
    public void setUp(){
        System.out.println("Init method called");
        game = new Game(true);
        logic = new GameLogic(true);
        Lobby.allLobbies.clear();
    }

    @Test
    public void CreateLobbyTest(){
        setUp();
        List<Lobby> lobbies = game.createLobby("name");
        assertEquals(1, lobbies.size());
    }

    @Test
    public void CreateLobbyWithInvalidNameTest(){
        setUp();
        List<Lobby> lobbies = game.createLobby(null);
        assertEquals(0, lobbies.size());
    }

    @Test
    public void PlayerCanJoinLobbyTest(){
        setUp();
        List<Lobby> lobbies = game.createLobby("Lobby1");
        Lobby joinLobby = new Lobby("joinLobby");
        joinLobby.getPlayers().add(new Player("Niels"));
        game.joinLobby(joinLobby);
        assertEquals(1, Lobby.allLobbies.get(0).getPlayers().size());
    }


    //TODO: FIX THIS TEST
    @Test
    public void SnakeMoveForwardTest() throws InterruptedException {
        setUp();
        List<Lobby> lobbies = game.createLobby("lobby1");
        Lobby joinLobby = new Lobby("joinLobby");
        joinLobby.getPlayers().add(new Player("Niels"));
        game.joinLobby(joinLobby);

        game.startGame(lobbies.get(0), 1, 1);

        Thread.sleep(100);
        GameData previousGameUpdate = game.getGameLogic().getLastGameUpdate();
        Thread.sleep(1500);
        GameData lastGameUpdate = game.getGameLogic().getLastGameUpdate();

        int previousXpos = previousGameUpdate.getPlayers().get(0).getSnakeTiles().get(0).getXPos();
        int newXpos = lastGameUpdate.getPlayers().get(0).getSnakeTiles().get(0).getXPos();
        boolean hasMoved = previousXpos < newXpos;
        assertEquals(true, hasMoved);
    }

    @Test
    public void SnakeCanChangeDirectionTest() throws InterruptedException {
        setUp();
        List<Lobby> lobbies = game.createLobby("lobby1");
        Lobby joinLobby = new Lobby("joinLobby");
        Player player = new Player("Niels");
        joinLobby.getPlayers().add(player);
        game.joinLobby(joinLobby);
        game.startGame(lobbies.get(0), 30, 15);
        Thread.sleep(80);
        game.changeDirection(Direction.UP, Lobby.allLobbies.get(0), player);
        Thread.sleep(80);
        GameData gameUpdate = game.getGameLogic().getLastGameUpdate();

        assertEquals(Direction.UP, gameUpdate.getPlayers().get(0).getDirection());
    }


    @Test
    public void SnakeLeavesLeftBorderTest() throws InterruptedException {
        setUp();
        List<Lobby> lobbies = game.createLobby("lobby1");
        Lobby joinLobby = new Lobby("joinLobby");
        Player player = new Player("Niels");
        joinLobby.getPlayers().add(player);
        game.joinLobby(joinLobby);
        game.startGame(lobbies.get(0), 2, 15);
        game.changeDirection(Direction.LEFT, Lobby.allLobbies.get(0), player);

        Thread.sleep(350);

        assertEquals(true, game.getGameLogic().getHasAPlayerDied());
    }


    @Test
    public void SnakeCanUpgradeTest() throws InterruptedException {
        setUp();
        List<Lobby> lobbies = game.createLobby("lobby1");
        Lobby joinLobby = new Lobby("joinLobby");
        Player player = new Player("Niels");
        joinLobby.getPlayers().add(player);
        game.joinLobby(joinLobby);
        game.startGame(lobbies.get(0), 2, 15);

        game.setLevelUp(new Tile(4, 15));

        Thread.sleep(300);
        int length = game.getGameLogic().getLastGameUpdate().getPlayers().get(0).getSnakeTiles().size();
        assertEquals(2, length);
    }

    @Test
    public void GameStartsAutomaticallyTest() throws InterruptedException, IOException {
        setUp();
        List<Lobby> lobbies = game.createLobby("lobby1");
        Lobby joinLobby = new Lobby("joinLobby");
        Player player = new Player("Niels");
        joinLobby.getPlayers().add(player);
        logic.joinLobby(joinLobby, null);

        joinLobby.getPlayers().add(new Player("player2"));
        logic.joinLobby(joinLobby, null);
        Thread.sleep(1500);
        logic.checkIfGameCanStart(joinLobby);
        Thread.sleep(1500);

        GameData data = logic.getGame().getGameLogic().getLastGameUpdate();

        boolean isDataNull = false;
        if (data == null){
            isDataNull = true;
        }
        assertFalse(isDataNull);
    }




    //TODO: HAS TO BE IMPLEMENTED CORRECTLY
    @Test
    public void SnakeHitsOwnBodyTest() throws InterruptedException {
        setUp();
        List<Lobby> lobbies = game.createLobby("lobby1");
        Lobby joinLobby = new Lobby("joinLobby");
        Player player = new Player("Niels");
        joinLobby.getPlayers().add(player);
        game.joinLobby(joinLobby);
        game.startGame(lobbies.get(0), 2, 15);
        game.setLevelUp(new Tile(3, 15));
        Thread.sleep(70);
        game.setLevelUp(new Tile(4,15));
        Thread.sleep(700);
        System.out.println(game.getGameLogic().getLastGameUpdate().getPlayers().get(0).getSnakeTiles().size());
        assertFalse(false);
    }





    @Test
    public void SnakeLeavesRightBorderTest() throws InterruptedException {
        setUp();
        List<Lobby> lobbies = game.createLobby("lobby1");
        Lobby joinLobby = new Lobby("joinLobby");
        Player player = new Player("Niels");
        joinLobby.getPlayers().add(player);
        game.joinLobby(joinLobby);
        game.startGame(lobbies.get(0), 54, 15);

        Thread.sleep(230);

        assertEquals(true, game.getGameLogic().getHasAPlayerDied());
    }

    @Test
    public void SnakeLeavesTopBorderTest() throws InterruptedException {
        setUp();
        this.game = new Game(true);
        this.logic = new GameLogic(true);
        List<Lobby> lobbies = game.createLobby("lobby1");
        Lobby joinLobby = new Lobby("joinLobby");
        Player player = new Player("Niels");
        joinLobby.getPlayers().add(player);
        game.joinLobby(joinLobby);
        game.startGame(lobbies.get(0), 30, 1);
        game.changeDirection(Direction.UP, Lobby.allLobbies.get(0), player);
        Thread.sleep(230);

        assertTrue(game.getGameLogic().getHasAPlayerDied());
    }

    @Test
    public void SnakeLeavesBottomBorderTest() throws InterruptedException {
        setUp();
        List<Lobby> lobbies = game.createLobby("lobby1");
        Lobby joinLobby = new Lobby("joinLobby");
        Player player = new Player("Niels");
        joinLobby.getPlayers().add(player);
        game.joinLobby(joinLobby);
        game.startGame(lobbies.get(0), 30, 54);
        game.changeDirection(Direction.DOWN, Lobby.allLobbies.get(0), player);
        Thread.sleep(230);

        assertEquals(true, game.getGameLogic().getHasAPlayerDied());
    }



}
