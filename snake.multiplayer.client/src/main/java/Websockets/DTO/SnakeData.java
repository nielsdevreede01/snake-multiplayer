package Websockets.DTO;

import Models.Direction;

public class SnakeData {
    int id;
    int size;
    Direction direction;

    public void setId(int id){
        this.id = id;
    }
    public void setSize(int size){
        this.size = size;
    }
    public void setDirection(Direction dir){
        this.direction = dir;
    }
    public int getId(){
        return id;
    }
    public int getSize(){
        return size;
    }
    public Direction getDirection() {
        return direction;
    }
}
