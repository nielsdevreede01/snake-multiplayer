package Websockets.DTO;

import Models.Player;
import Models.Tile;

import java.util.List;

public class GameData {
    private List<Player> players;
    private Tile levelUp;


    public List<Player> getPlayers(){
        return this.players;
    }
    public Tile getLevelUp(){
        return this.levelUp;
    }
}
