package Websockets.DTO;

import Models.Direction;
import Models.Lobby;
import Models.Player;

import java.nio.file.DirectoryStream;

public class DirectionData {
    Player currentPlayer = Player.getCurrentPlayer();
    Direction dir;
    Lobby lobby;
    public DirectionData (Direction dir, Lobby lobby){
        this.dir = dir;
        this.lobby = lobby;
    }
}
