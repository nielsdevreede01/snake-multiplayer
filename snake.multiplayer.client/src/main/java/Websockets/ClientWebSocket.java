package Websockets;

import Models.Direction;
import Models.Lobby;
import Models.Player;
import Websockets.DTO.DirectionData;
import Websockets.DTO.GameData;
import Websockets.DTO.SnakeData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import main.MultiplayerGame;

import javax.websocket.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ClientEndpoint
public class ClientWebSocket {
    private final String uri = "ws://localhost:9005/game/";
    private Session session;
    private String message;
    private Gson gson;
    private MultiplayerGame game;

    private ClientWebSocket(MultiplayerGame game) {
        this.game = game;
        gson = new Gson();
    }

    public void startConnection() {
        if (session == null) {
            try {
                System.out.println("Trying to connect to server");
                WebSocketContainer container = ContainerProvider.getWebSocketContainer();
                this.session = container.connectToServer(this, new URI(uri));
            } catch (DeploymentException | IOException | URISyntaxException e) {
                System.err.println("[ERROR] " + e.getMessage());
            }
        } else {
            System.err.println("[Connection Already Started]");
        }
    }

    private static ClientWebSocket instance = null;

    public static ClientWebSocket getInstance(MultiplayerGame game) {
        if (instance == null) {
            instance = new ClientWebSocket(game);
        }
        return instance;
    }

    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
        System.out.println("[Connection opened]  " + this.session.getId());
    }

    @OnError
    public void onError(Session session, Throwable cause) {
        System.out.println("[Connection error]  " + cause);
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("[Websocket message] " + message);
        this.message = message;

        handleMessage(message, session);
    }

    @OnClose
    public void onClose(Session session) {
        System.out.println("[Connection closed]");
        this.message = null;
        this.session = null;
    }

    private void handleMessage(String message, Session session) {
        WebSocketMessage msg = gson.fromJson(message, WebSocketMessage.class);

        if (msg.getStatus() != WebSocketStatus.ERROR) {
            switch (msg.getAction()) {
                case CREATE_LOBBY:
                    String data = msg.getData();
                    List<Lobby> lobbies = gson.fromJson(data, new TypeToken<ArrayList<Lobby>>() {
                    }.getType());
                    game.updateLobbies(lobbies);
                    break;
                case PLAYER_JOINED:
                    data = msg.getData();
                    Lobby lobby = gson.fromJson(data, Lobby.class);
                    game.playerJoinedLobby(lobby);
                    break;
                case OTHER_PLAYER_JOINED_LOBBY:
                    data = msg.getData();
                    lobby = gson.fromJson(data, Lobby.class);
                    game.otherPlayerJoinedLobby(lobby);
                    break;
                case START_GAME:
                    data = msg.getData();
                    lobby = gson.fromJson(data, Lobby.class);
                    game.startGame(lobby);
                    break;
                case GAME_UPDATE:
                    data = msg.getData();
                    GameData gameData = gson.fromJson(data, GameData.class);
                    game.updateGame(gameData);
                    break;
                case GET_LOBBIES:
                    data = msg.getData();
                    lobbies = gson.fromJson(data, new TypeToken<ArrayList<Lobby>>() {
                    }.getType());
                    game.updateLobbies(lobbies);
            }
        } else {
            System.err.println("[RECEIVED ERROR STATUS] + " + msg.getMessage());
        }
    }

    public void createLobby(String name) throws IOException {
        WebSocketMessage msg = new WebSocketMessage(WebSocketAction.CREATE_LOBBY, null, null, gson.toJson(name));
        session.getAsyncRemote().sendText(gson.toJson(msg));
    }

    public void joinLobby(Lobby lobby) {
        Player currentPlayer = Player.getCurrentPlayer();
        lobby.setPlayers(currentPlayer);

        WebSocketMessage msg = new WebSocketMessage(WebSocketAction.JOIN_LOBBY, null, null, gson.toJson(lobby));
        session.getAsyncRemote().sendText(gson.toJson(msg));
    }

    public void changeDirection(Direction direction, Lobby lobby) throws IOException {
        DirectionData dirData = new DirectionData(direction, lobby);
        WebSocketMessage msg = new WebSocketMessage(WebSocketAction.CHANGE_DIRECTION, null, null, gson.toJson(dirData));
        session.getBasicRemote().sendText(gson.toJson(msg));
    }

    public void getLobbies() {
        WebSocketMessage msg = new WebSocketMessage(WebSocketAction.GET_LOBBIES, null, null, null);
        session.getAsyncRemote().sendText(gson.toJson(msg));
    }
}
