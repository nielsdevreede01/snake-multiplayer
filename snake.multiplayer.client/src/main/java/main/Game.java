package main;

import Models.Direction;
import Models.Snake;
import Models.Tile;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.paint.Color;

import java.util.*;

public class Game {
    private Main application;
    private Snake player = new Snake();
    private final int SPEED = 75;
    private int GRID_SIZE;
    private Tile currentLevelUp;
    private Timer timer;


    public Game(Main application, int gridSize) {
        this.application = application;
        this.GRID_SIZE = gridSize;
        player.addTileToSnake(GRID_SIZE / 2, GRID_SIZE / 2);
        createLevelUp();
        TimerTask task = new TimerTask() {
            public void run() {
                updateTimer();
            }
        };
        timer = new Timer();
        timer.scheduleAtFixedRate(task, SPEED, SPEED);
    }

    private void updateTimer() {
        Tile p = player.getFirstTile();
        Tile c = currentLevelUp;
        if (player.getFirstTile().getXPos() == currentLevelUp.getXPos() && player.getFirstTile().getYPos() == currentLevelUp.getYPos()) {
            player.addTileToSnake();
            application.updateScore(player.getSize());
            createLevelUp();
        }

        player.moveForward();
        if (!checkIfPlayerIsAlive()) {
            application.playerDied();
            timer.cancel();
        }
        application.updateScreen(player.getPreviousLastTile(), player.getFirstTile(), player.getSize() - 1);
    }

    public void changeDirection(Direction dir) {
        player.changeDirection(dir);
    }
    private Random rnd = new Random();
    public void createLevelUp() {
        int xPos = rnd.nextInt(GRID_SIZE - 1);
        int yPos = rnd.nextInt(GRID_SIZE - 1);
        this.currentLevelUp = new Tile(xPos, yPos);
        application.setLevelUp(xPos, yPos);
    }

    private boolean checkIfPlayerIsAlive() {
        List<Tile> allSnakeTiles = player.getSnakeTiles();
        Tile firstTile = player.getFirstTile();
        int index = 0;
        for (Tile bodyTile: allSnakeTiles) {
            if (bodyTile.getYPos() == firstTile.getYPos() &&
                    bodyTile.getXPos() == firstTile.getXPos() &&
                    index != 0){
                return false;
            }
            index++;
        }

        return !checkIfPlayerIsOutSideBounds();
    }

    private boolean checkIfPlayerIsOutSideBounds() {
        Tile firstTile = player.getFirstTile();
        System.out.println(firstTile.getXPos());
        return firstTile.getXPos() > GRID_SIZE - 1 ||
                firstTile.getXPos() < 0 ||
                firstTile.getYPos() > GRID_SIZE - 1 ||
                firstTile.getYPos() < 0;
    }
}
