package main;

import Models.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.util.List;

public class Main extends Application {

    MultiplayerGame mpGame;
    Scene loginScene, lobbyScene, gameScene;

    private Game game;
    private int GRID_SIZE = 55;
    private int VIEW_SIZE = 825;
    private Rectangle[][] rectangles = new Rectangle[GRID_SIZE][GRID_SIZE];
    private Text score;
    private Stage stage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        //game = new Game(this, GRID_SIZE);
        stage = primaryStage;
        startLoginScene();
    }

    private void startSinglePlayerGameScene() {
        GridPane gridPane = setGameGridPane();
        stage.setTitle("Snake Game");
        gameScene = new Scene(gridPane);
        stage.setScene(gameScene);
        stage.show();
        game = new Game(this, GRID_SIZE);


        gameScene.setOnKeyPressed(e -> {
            switch (e.getCode()) {
                case LEFT:
                    game.changeDirection(Direction.LEFT);
                    break;
                case RIGHT:
                    game.changeDirection(Direction.RIGHT);
                    break;
                case DOWN:
                    game.changeDirection(Direction.DOWN);
                    break;
                case UP:
                    game.changeDirection(Direction.UP);
            }
        });
    }

    private ObservableList<Lobby> lobbyList;

    private void startLobbyScene() {
        mpGame = new MultiplayerGame(this);
        GridPane gridPane = new GridPane();
        gridPane.setMinSize(600, 600);
        stage.setTitle("Available lobbies");
        gridPane.setAlignment(Pos.CENTER);


        lobbyList = FXCollections.observableArrayList();

        ListView lobbyListView = new ListView<>();
        Button btnJoin = new Button("Join lobby");
        Button btnCreate = new Button("Create lobby");
        Button btnSingle = new Button("Start a single player game");
        GridPane.setHalignment(btnCreate, HPos.CENTER);
        GridPane.setHalignment(btnJoin, HPos.LEFT);
        GridPane.setHalignment(btnSingle, HPos.RIGHT);
        gridPane.add(lobbyListView, 1, 1);
        gridPane.add(btnJoin, 1, 2);
        gridPane.add(btnCreate, 1, 2);
        gridPane.add(btnSingle, 2, 2);
        lobbyScene = new Scene(gridPane);
        stage.setScene(lobbyScene);
        stage.show();

        lobbyListView.setItems(lobbyList);

        lobbyListView.setCellFactory(param -> new ListCell<Lobby>() {
            @Override
            protected void updateItem(Lobby item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getName() == null) {
                    setText(null);
                } else {
                    setText(item.getName());
                }
            }
        });

        mpGame.getLobbies();

        btnSingle.setOnAction(ActionEvent -> {
            stage.close();
            startSinglePlayerGameScene();
        });

        btnCreate.setOnAction(ActionEvent -> {
            String name = JOptionPane.showInputDialog("Enter name of the lobby");
            try {
                mpGame.createLobby(name);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnJoin.setOnAction(ActionEvent -> {
            Lobby joinLobby = (Lobby) lobbyListView.getSelectionModel().getSelectedItem();
            mpGame.joinLobby(joinLobby);
        });
    }

    public void updateLobbies(List<Lobby> lobby) {
        Platform.runLater(() -> {
            lobbyList.clear();
            lobbyList.addAll(lobby);
        });
    }

    private void startLoginScene() {
        GridPane gridPane = new GridPane();
        gridPane.setMinSize(300, 300);
        stage.setTitle("Please login");
        gridPane.setAlignment(Pos.CENTER);

        TextField usernameTA = new TextField("Niels01@gmail.com");
        PasswordField passwordTA = new PasswordField();
        passwordTA.setText("password123");
        Button loginBtn = new Button("LOGIN");
        GridPane.setFillHeight(loginBtn, true);
        gridPane.add(usernameTA, 0, 1);
        gridPane.add(passwordTA, 0, 2);
        gridPane.add(loginBtn, 0, 3);

        loginBtn.setOnMouseClicked(actionEvent -> {
            Authorization auth = new Authorization();
            try {
                if (auth.login(usernameTA.getText(), passwordTA.getText())) {
                    stage.close();
                    startLobbyScene();
                } else {
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Login Failed");
                        alert.setHeaderText("Error");
                        alert.setContentText("Username or password is incorrect");
                        alert.show();
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        loginScene = new Scene(gridPane);
        stage.setScene(loginScene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void updateScreen(Tile lastTile, Tile firstTile, int size) {
        rectangles[lastTile.getXPos()][lastTile.getYPos()].setFill(Color.BLACK);
        rectangles[firstTile.getXPos()][firstTile.getYPos()].setFill(Color.RED);
    }

    public void updateScore(int score) {
        this.score.setText(Integer.toString(score));
    }

    public void setLevelUp(int xpos, int ypos) {
        rectangles[xpos][ypos].setFill(Color.PURPLE);
    }

    public void playerDied() {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Player is dead");
            alert.setHeaderText("Player has died");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    resetAllRectangleColors();
                    game = new Game(this, GRID_SIZE);
                    score.setText("0");
                }
            });
        });
    }

    private void resetAllRectangleColors() {
        for (int x = 0; x < GRID_SIZE; x++) {
            for (int y = 0; y < GRID_SIZE; y++) {
                rectangles[x][y].setFill(Color.BLACK);
            }
        }
    }

    public void playerJoinedLobby(Lobby lobby) {
        GridPane gridPane = new GridPane();
        gridPane.setMinSize(600, 600);
        stage.setTitle(lobby.getName());
        gridPane.setAlignment(Pos.CENTER);

        ListView lobbyListView = new ListView<>();
        for (Player player : lobby.getPlayers()) {
            lobbyListView.getItems().add(player.getName() + "  is ready");
        }

        gridPane.add(lobbyListView, 1, 1);
        Scene joinedLobbyScene = new Scene(gridPane);
        stage.setScene(joinedLobbyScene);
        stage.show();
    }

    public void openMpGame(Lobby lobby) {
        GridPane gridPane = setGameGridPane();

        Scene mpGameScene = new Scene(gridPane);
        mpGameScene.setOnKeyPressed(e -> {
            try {
                switch (e.getCode()) {
                    case LEFT:
                        mpGame.changeDirection(Direction.LEFT);
                        break;
                    case RIGHT:
                        mpGame.changeDirection(Direction.RIGHT);
                        break;
                    case DOWN:
                        mpGame.changeDirection(Direction.DOWN);
                        break;
                    case UP:
                        mpGame.changeDirection(Direction.UP);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        stage.setTitle("Snake Game");
        stage.setScene(mpGameScene);
        stage.show();
    }

    private GridPane setGameGridPane() {
        GridPane gridPane = new GridPane();
        gridPane.setMinSize(VIEW_SIZE, VIEW_SIZE);

        for (int y = 0; y < GRID_SIZE; y++) {
            for (int x = 0; x < GRID_SIZE; x++) {
                Rectangle rec = new Rectangle(VIEW_SIZE / (double) GRID_SIZE, VIEW_SIZE / (double) GRID_SIZE);
                gridPane.add(rec, x, y);
                rectangles[x][y] = rec;
            }
        }

        score = new Text();
        score.setText("0");
        score.setFill(Color.WHITE);
        gridPane.getChildren().add(score);
        return gridPane;
    }

    public void updateMpGame(Snake ownSnake, Snake otherSnake, Tile levelUp) {
        resetAllRectangleColors();
        for (Tile tile : ownSnake.getSnakeTiles()) {
            rectangles[tile.getXPos()][tile.getYPos()].setFill(Color.RED);
        }
        for (Tile tile : otherSnake.getSnakeTiles()) {
            rectangles[tile.getXPos()][tile.getYPos()].setFill(Color.BLUE);
        }

        rectangles[levelUp.getXPos()][levelUp.getYPos()].setFill(Color.PURPLE);
    }
}
