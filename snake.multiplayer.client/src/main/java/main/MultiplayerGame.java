package main;

import Models.*;
import Websockets.ClientWebSocket;
import Websockets.DTO.GameData;
import javafx.application.Platform;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MultiplayerGame {
    List<Snake> snakes = new ArrayList<>();
    private Tile powerUpTile;
    private Main application;
    private Lobby lobby;

    private ClientWebSocket communicator;

    public MultiplayerGame(Main application) {
        this.application = application;
        communicator = ClientWebSocket.getInstance(this);
        communicator.startConnection();
    }

    public void updateLobbies(List<Lobby> lobby) {
        this.application.updateLobbies(lobby);
    }

    public void createLobby(String name) throws IOException {
        communicator.createLobby(name);
    }

    public void joinLobby(Lobby lobby) {

        communicator.joinLobby(lobby);
    }

    public void playerJoinedLobby(Lobby lobby) {

        Platform.runLater(() -> {
            application.playerJoinedLobby(lobby);
        });
    }

    public void otherPlayerJoinedLobby(Lobby lobby) {
        Platform.runLater(() -> {
            application.playerJoinedLobby(lobby);
        });
    }

    public void changeDirection(Direction direction) throws IOException {
        communicator.changeDirection(direction, lobby);
    }

    public void startGame(Lobby lobby) {
        this.lobby = lobby;
        for (Player plyr : lobby.getPlayers()) {
        if (plyr.getName().equals(Player.getCurrentPlayer().getName())) {
            Player.setCurrentPlayer(plyr);
        }
    }
        Platform.runLater(() -> {
            application.openMpGame(lobby);
        });
    }

    public void updateGame(GameData gameData) {
        System.err.println("OWN ID: " + Player.getCurrentPlayer().getId());
        Snake ownSnake = null;
        Snake otherSnake = null;
        for (Player player : gameData.getPlayers()) {
            if (player.getId() == (Player.getCurrentPlayer().getId())) {
                ownSnake = player.getSnake();
            } else {
                otherSnake = player.getSnake();
            }
        }
        Tile levelUp = gameData.getLevelUp();

        application.updateMpGame(ownSnake, otherSnake, levelUp);
    }

    public void getLobbies() {
        communicator.getLobbies();
    }
}
