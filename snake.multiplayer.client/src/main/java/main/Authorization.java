package main;

import Models.Player;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultClientConnection;
import org.eclipse.jetty.websocket.api.StatusCode;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Authorization {

    private CloseableHttpClient httpClient = HttpClients.createDefault();
    private final String apiBaseURL = "http://localhost:1618/api/";
    private Gson gson;

    public Authorization(){
        gson = new Gson();
    }

    public boolean login(String usrName, String psWord) throws IOException {

        try{
            HttpClient httpClient = null;
        }finally {
            httpClient = new DefaultHttpClient();
        }
        try{

        }catch (Exception e){
            System.err.println(e.getMessage());
        }

        Map map = new HashMap();
        map.put("password", psWord);
        StringEntity requestEntity = new StringEntity(gson.toJson(map), ContentType.APPLICATION_JSON);
        HttpPost post = new HttpPost(apiBaseURL + "login/" + usrName);
        post.setEntity(requestEntity);
        HttpResponse rawResponse = httpClient.execute(post);

        int responseCode = rawResponse.getStatusLine().getStatusCode();
        System.out.println(responseCode);
        if (responseCode == HttpStatus.SC_OK){
            Player player = new Player();
            player.setName(usrName);
            Player.setCurrentPlayer(player);
            return true;
        }
        return false;


    }
}
