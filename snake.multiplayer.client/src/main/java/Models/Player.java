package Models;

public class Player {
    private static Player currentPlayer;
    public static Player getCurrentPlayer(){
        return currentPlayer;
    }
    public static void setCurrentPlayer(Player player){
        currentPlayer = player;
    }

    private int id;
    private Snake snake;
    private String name;


    public void setName(String name) {
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public Snake getSnake() {
        return this.snake;
    }
    public int getId(){
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
}
