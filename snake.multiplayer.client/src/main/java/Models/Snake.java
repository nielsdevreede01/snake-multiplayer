package Models;

import java.util.ArrayList;
import java.util.List;

public class Snake {
    private int id;
    private List<Tile> tiles = new ArrayList<>();
    private Direction direction = Direction.RIGHT;
    private Tile previousLastTile;
    private Tile previousFirstTile;

    public List<Tile> getSnakeTiles() {
        return this.tiles;
    }
    public Direction getDirection() {
        return this.direction;
    }
    public void addTileToSnake(int xPos, int yPos) {
        tiles.add(new Tile(xPos, yPos));
        this.previousFirstTile = tiles.get(0);
    }
    public void addTileToSnake() {
        switch (direction) {
            case LEFT:
                tiles.add(new Tile(previousLastTile.getXPos() + 1, previousLastTile.getYPos()));
                break;
            case DOWN:
                tiles.add(new Tile(previousLastTile.getXPos(), previousLastTile.getYPos() - 1));
                break;
            case UP:
                tiles.add(new Tile(previousLastTile.getXPos(), previousLastTile.getYPos() + 1));
                break;
            case RIGHT:
                tiles.add(new Tile(previousLastTile.getXPos() - 1, previousLastTile.getYPos()));
                break;
            default:
                throw new IllegalArgumentException();
        }
    }
    public void changeDirection(Direction dir) {
        this.direction = dir;
    }
    public void moveForward() {
        switch (direction) {
            case UP:
                this.previousLastTile = tiles.get(tiles.size() - 1);
                this.previousFirstTile = tiles.get(0);
                this.tiles.add(0, new Tile(previousFirstTile.getXPos(), previousFirstTile.getYPos() - 1));
                this.tiles.remove(tiles.size() - 1);
                this.previousFirstTile = tiles.get(0);
                break;
            case DOWN:
                this.previousLastTile = tiles.get(tiles.size() - 1);
                this.previousFirstTile = tiles.get(0);
                this.tiles.add(0, new Tile(previousFirstTile.getXPos(), previousFirstTile.getYPos() + 1));
                this.tiles.remove(tiles.size() - 1);
                this.previousFirstTile = tiles.get(0);
                break;
            case LEFT:
                this.previousLastTile = tiles.get(tiles.size() - 1);
                this.previousFirstTile = tiles.get(0);
                this.tiles.add(0, new Tile(previousFirstTile.getXPos() - 1, previousFirstTile.getYPos()));
                this.tiles.remove(tiles.size() - 1);
                this.previousFirstTile = tiles.get(0);
                break;
            case RIGHT:
                this.previousLastTile = tiles.get(tiles.size() - 1);
                this.previousFirstTile = tiles.get(0);
                this.tiles.add(0, new Tile(previousFirstTile.getXPos() + 1, previousFirstTile.getYPos()));
                this.tiles.remove(tiles.size() - 1);
                this.previousFirstTile = tiles.get(0);
                break;
            default:
                throw new IllegalArgumentException();
        }
    }
    public Tile getPreviousLastTile() {
        return previousLastTile;
    }
    public Tile getFirstTile() {
        return previousFirstTile;
    }
    public int getSize() {
        return this.tiles.size();
    }
    public void setId(int id){this.id = id;}
    public int getId(){return this.id;}

}
