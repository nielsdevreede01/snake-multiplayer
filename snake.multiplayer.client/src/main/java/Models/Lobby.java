package Models;

import java.util.ArrayList;
import java.util.List;

public class Lobby {
    private String name;
    private int id;
    List<Player> players = new ArrayList<>();

    public String getName() {
        return this.name;
    }
    public int getId(){
        return this.id;
    }
    public void setPlayers(Player player){
        this.players.add(player);
    }

    public List<Player> getPlayers(){
        return this.players;
    }
}
