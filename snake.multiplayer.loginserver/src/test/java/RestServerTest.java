import Controllers.AuthorizationRepository;
import Database.IAuthorizationDatabaseContext;
import Database.InMemoryAuthorizationContext;
import Models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class RestServerTest {
    IAuthorizationDatabaseContext db;

    @BeforeEach
    public void setup() throws IOException {
        this.db = new AuthorizationRepository(new InMemoryAuthorizationContext());
    }

    @Test
    public void testGetUserFromDB() throws Exception {
        String username = "Niels";
        String password = "password123";

        User user = db.getUserFromDb(username);
        boolean isUserNull = user == null;
        assertFalse(isUserNull);
    }

    @Test
    public void testGetUserFromDBNullData() throws Exception {
        String username = null;
        String password = null;

        User user = db.getUserFromDb(username);
        boolean isUserId0 = user.getId() == 0;
        assertTrue(isUserId0);
    }

    @Test
    public void testCreateNewUser() throws Exception {
        String username = "Jan";
        String password = "password123";

        db.createUser(username, password);

        User user = db.getUserFromDb(username);
        assertEquals("Jan", user.getEmail());
        assertEquals("password123", user.getPassword());
    }

    @Test
    public void testCreateNewUserDouble() throws Exception {
        String username = "Jan";
        String password = "password123";

        db.createUser(username, password);
        db.createUser(username, password);

        User user = db.getUserFromDb(username);
        assertEquals("Jan", user.getEmail());
        assertEquals("password123", user.getPassword());
    }


    @Test
    public void testCreateNewUserNullData() throws Exception {
        String username = null;
        String password = null;

        db.createUser(username, password);

        User user = db.getUserFromDb(username);
        assertNull(user.getEmail());
        assertNull(user.getPassword());
        assertEquals(0, user.getId());
    }


    @Test
    public void testDeleteUser() throws Exception {
        db.deleteUser("Niels");

        User user = db.getUserFromDb("Niels");
        assertNull(user);
    }

    @Test
    public void testUpdateUser() throws Exception {
        int id = 1;
        String email = "NielsNew";
        String password = "password12435";

        db.updateUser(id, email, password);

        User user = db.getUserFromDb(email);
        assertEquals(email, user.getEmail());
        assertEquals(password, user.getPassword());
    }
}
