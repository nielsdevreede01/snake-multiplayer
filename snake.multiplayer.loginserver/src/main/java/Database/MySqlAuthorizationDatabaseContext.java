package Database;

import Models.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class MySqlAuthorizationDatabaseContext implements IAuthorizationDatabaseContext {

    InputStream input = new FileInputStream("C:\\Users\\Niels\\Desktop\\School\\Semester 3\\Big_Idea\\SnakeMultiplayer\\snake.multiplayer.loginserver\\src\\main\\java\\Database\\config.properties");
    String dbUser;
    String dbPassw;
    String dbLocation;
    Connection conn;

    public MySqlAuthorizationDatabaseContext() throws IOException {
        Properties prop = new Properties();
        prop.load(input);
        this.dbLocation = prop.getProperty("db.url");
        this.dbUser = prop.getProperty("db.user");
        this.dbPassw = prop.getProperty("db.password");
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(dbLocation, dbUser, dbPassw);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    private PreparedStatement ps1;
    @Override
    public User getUserFromDb(String email) throws Exception {
        User usr = new User();
        ResultSet rs = null;

        try (PreparedStatement ps = conn.prepareStatement("select * from users WHERE email = ?")) {
            ps1 = ps;
            ps1.setString(1, email);
            rs = ps1.executeQuery();
            rs.next();
            usr.setId(rs.getInt(1));
            usr.setEmail(rs.getString(2));
            usr.setPassword(rs.getString(3));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new Exception();
        } finally {
            assert rs != null;
            rs.close();
        }
        return usr;
    }

    @Override
    public void deleteUser(String email) throws Exception {
        try (PreparedStatement ps = conn.prepareStatement("DELETE from users WHERE email = ?");) {
            ps.setString(1, email);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new Exception();
        }
    }

    @Override
    public void updateUser(int id, String email, String password) throws Exception {
        try (PreparedStatement ps = conn.prepareStatement("UPDATE `users` SET `email`=?,`password`=? WHERE id  = ?");
        ) {
            ps.setString(1, email);
            ps.setString(2, password);
            ps.setInt(3, id);
            ps.execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new Exception();
        }
    }

    @Override
    public void createUser(String email, String password) throws Exception {
        try (PreparedStatement ps = conn.prepareStatement("INSERT INTO `users`(`id`, `email`, `password`) VALUES (null,?,?)");
        ) {
            ps.setString(1, email);
            ps.setString(2, password);
            ps.execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new Exception();
        }
    }
}
