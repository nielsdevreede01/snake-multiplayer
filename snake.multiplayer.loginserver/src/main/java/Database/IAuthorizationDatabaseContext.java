package Database;

import Models.User;

import java.sql.SQLException;

public interface IAuthorizationDatabaseContext {

    User getUserFromDb(String email) throws Exception;
    void deleteUser(String email) throws Exception;
    void updateUser(int id, String email, String password) throws Exception;
    void createUser(String email, String password) throws Exception;

}
