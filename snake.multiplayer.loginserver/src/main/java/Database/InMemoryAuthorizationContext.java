package Database;

import Models.User;

import java.util.ArrayList;
import java.util.List;

public class InMemoryAuthorizationContext implements IAuthorizationDatabaseContext{
    private List<User> userList = new ArrayList<>();
    private int idCount = 1;


    public InMemoryAuthorizationContext(){
        User user = new User();
        user.setId(idCount++);
        user.setPassword("password123");
        user.setEmail("Niels");
        userList.add(user);
    }

    @Override
    public User getUserFromDb(String email) throws Exception {
        for(User user: userList){
            if (user.getEmail().equals(email)){
                return user;
            }
        }
        return null;
    }

    @Override
    public void deleteUser(String email) throws Exception {
        User removeUser = null;
        for (User user : userList){
            if (user.getEmail().equals(email)){
                removeUser = user;
                break;
            }
        }
        userList.remove(removeUser);
    }

    @Override
    public void updateUser(int id, String email, String password) throws Exception {
        for(User user: userList){
            if (user.getId() == id){
                user.setPassword(password);
                user.setEmail(email);
            }
        }
    }

    @Override
    public void createUser(String email, String password) throws Exception {
        if (!userList.stream().filter(o -> o.getEmail().equals(email)).findFirst().isPresent()){
            User user = new User();
            user.setEmail(email);
            user.setPassword(password);
            user.setId(idCount++);
            userList.add(user);
        }
    }
}
