package Models;

public class User {
    private String email;
    private int id;
    private String password;

    public void setId(int id) { this.id = id;}
    public void setEmail(String email) {this.email = email;}
    public void setPassword(String password) {this.password = password;}

    public int getId() {
        return id;
    }
    public String getEmail() {
        return email;
    }
    public String getPassword() {
        return password;
    }
}
