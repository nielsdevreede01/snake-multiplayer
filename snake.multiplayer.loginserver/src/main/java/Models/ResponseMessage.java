package Models;

import com.google.gson.Gson;

import javax.ws.rs.core.Response;
import java.util.Map;

public class ResponseMessage {

    Map message;

    public ResponseMessage(Map message){
        this.message = message;
    }

    public String getMessage() {
        Gson gson = new Gson();
        return gson.toJson(message);
    }
}
