package Controllers;

import Database.MySqlAuthorizationDatabaseContext;
import Database.IAuthorizationDatabaseContext;
import Models.ResponseMessage;
import Models.User;
import com.google.gson.Gson;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


@Path("/api")
public class AuthorizationController {

    private IAuthorizationDatabaseContext db;
    private Gson gson = new Gson();

    public AuthorizationController() throws IOException {
        db = new AuthorizationRepository();
    }


    @POST
    @Path("/login/{user_email}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(@PathParam("user_email") String user_email, InputStream inputUser) throws Exception {
        int responseStatus = 200;
        Map map = new HashMap();
        Properties data = null;
        try {
            data = gson.fromJson(getStringFromInputStream(inputUser), Properties.class);
            String inputPassword = data.getProperty("password");
            User user = db.getUserFromDb(user_email);
            if (user.getId() != 0) {
                if (user.getPassword().equals(inputPassword)) {
                    map.put("user_email", user_email);
                }else{
                    responseStatus = 401;
                }
            }else{
                responseStatus = 404;
            }
        }catch (Exception e){
            System.err.println("[SERVER ERROR]");
            responseStatus = 400;
        }
        ResponseMessage response = new ResponseMessage(map);
        return Response.status(responseStatus).entity(response.getMessage()).build();
    }

    @DELETE
    @Path("/delete/{user_email}")
    public Response delete(@PathParam("user_email") String user_email) throws SQLException {

        int responseStatus = 200;
        Map map = new HashMap();

        try{
            db.deleteUser(user_email);
        }catch (Exception e){
            System.out.println(e.getMessage());
            responseStatus = 400;
        }

        ResponseMessage response = new ResponseMessage(map);
        return Response.status(responseStatus).entity(response.getMessage()).build();
    }

    @POST
    @Path("/create")
    public Response create(InputStream input){

        int responseStatus = 200;
        Map map = new HashMap();

        Properties data = gson.fromJson(getStringFromInputStream(input), Properties.class);
        String inputPassword = data.getProperty("password");
        String inputEmail = data.getProperty("email");

        try{
            db.createUser(inputEmail, inputPassword);
        }catch (Exception e){
            System.out.println(e.getMessage());
            responseStatus = 400;
        }

        ResponseMessage response = new ResponseMessage(map);
        return Response.status(responseStatus).entity(response.getMessage()).build();
    }

    @POST
    @Path("/update/{user_id}")
    public Response update(@PathParam("user_id") int user_id, InputStream input){

        int responseStatus = 200;
        Map map = new HashMap();

        Properties data = gson.fromJson(getStringFromInputStream(input), Properties.class);
        String inputPassword = data.getProperty("password");
        String inputEmail = data.getProperty("email");

        try{
            db.updateUser(user_id, inputEmail ,inputPassword);
        }catch (Exception e){
            System.out.println(e.getMessage());
            responseStatus = 400;
        }

        ResponseMessage response = new ResponseMessage(map);
        return Response.status(responseStatus).entity(response.getMessage()).build();
    }


    private String getStringFromInputStream(InputStream i ) {
        InputStreamReader isReader = new InputStreamReader(i);
        BufferedReader reader = new BufferedReader(isReader);
        StringBuilder sb = new StringBuilder();
        String str = null;
        while(true){
            try {
                if ((str = reader.readLine()) == null) break;
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
            sb.append(str);
        }
        return sb.toString();
    }
}

