package Controllers;

import Database.IAuthorizationDatabaseContext;
import Database.MySqlAuthorizationDatabaseContext;
import Models.User;

import java.io.IOException;

public class AuthorizationRepository implements IAuthorizationDatabaseContext {

    private IAuthorizationDatabaseContext db;

    public AuthorizationRepository() throws IOException {
        db = new MySqlAuthorizationDatabaseContext();
    }

    public AuthorizationRepository(IAuthorizationDatabaseContext context) throws IOException {
        db = context;
    }


    @Override
    public User getUserFromDb(String email) throws Exception {
        if (email == null) {
            User user = new User();
            user.setId(0);
            return user;
        }
        return db.getUserFromDb(email);
    }

    @Override
    public void deleteUser(String email) throws Exception {
        db.deleteUser(email);
    }

    @Override
    public void updateUser(int id, String email, String password) throws Exception {
        if (id != 0 && email != null && password != null)
            db.updateUser(id, email, password);
    }

    @Override
    public void createUser(String email, String password) throws Exception {
        if (email != null && password != null)
            db.createUser(email, password);
    }
}
